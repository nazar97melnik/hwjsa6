"use strict";
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// Я це розумію так що є функції які виконуються одразу, одна за одною , а в контексті асинхронності ми не можемо просто створити , наприклад , функцію, яка буде виконуватись одразу після першої функції і гарантувати, що вона не поверне нам undefined , так як ми працюємо з сервером ми не можемо бути точно впевнені що за 1мс ми отримаємо ту інформацію, на яку чекаємо, і наступна функція буде працювати належним чином , за допомогою слів async await ця проблема зникає, ми можемо передавати отримані нами результати, робити якісь маніпуляції з отриманим результатом , тому що прописавши await ми точно знаємо, що JS на цьому місці зупиниться, знайде те, що ми туди передали і далі вже буде читати код як завжди.
const searchById = document.querySelector("#searchById");
searchById.addEventListener("click", findLocation);

async function findLocation() {
  try {
    const promise = await fetch("https://api.ipify.org/?format=json");
    if (promise.status === 200) {
      const data = await promise.json();
      const { ip } = data;
      await getInfo(ip);
    } else {
      throw new Error("Something wrong with server or with your validation");
    }
  } catch (error) {
    alert(error);
  }
}

async function getInfo(ip) {
  const promise = await fetch(`http://ip-api.com/json/${ip}`);

  if (!promise.ok) {
    throw new Error("Something went wrong with the server.");
  }
  const info = await promise.json();
  const { country, city, zip } = info;
  const resultDiv = document.createElement("div");
  resultDiv.innerHTML = `
  <p>Ми тебе знайшли!</p>
  <p>Твоя країна : ${country}</p>
  <p>Твоє місто : ${city}</p>
  <p>Твій поштовий індекс: ${zip}</p>
  `;
  document.body.append(resultDiv);
  if (info) {
    alert("Ось хто мою мамку в контер страйку обзивав!");
  }
}
